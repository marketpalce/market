//core modules
var path = require("path");
var fs = require("fs");
var zlib = require("zlib");
//core modules

///

//installed modules
var through2 = require("through2");
//installed modules

///

//our utilities
//ADJUST THE PATH OF 'logger.js' ACCORDING TO THE LOCATION OF YOUR NEW FILE
var logger = require("./Logger.js").logMaker;
var sm = require("./sessionManager.js");
//our utilities

///

//global variables
//variables that will be used through out the file, such as db table names, or file paths, etc...
//global variables

///


var getStaticFile = function getStaticFileFn(filepath, req, res) {

	var gzipper = new zlib.Gzip;
	
	gzipper.on("error", function(err) {
		logger(null, 1, "SS-StaticFile", "Error compressing file");
	});

	var ext=[".htm", ".html", ".js", ".css", ".txt", ".png", ".jpeg", ".jpg", ".gif", ".bmp", ".tif", ".tiff", ".ico", ".woff", ".xml"];

	var cType=["text/html", "text/html", "text/javascript", "text/css", "text/plain", "image/png", "image/jpeg", "image/jpeg", "image/gif", "image/bmp", "image/tiff", "image/tiff", "image/x-icon", "application/x-font-woff", "application/xml"];

	filepath=path.normalize(filepath);

	var fileExtension = path.extname(filepath);

	var fileData = filepath.split("/");

	var filename = fileData[fileData.length-1];


	if( filename.charAt(0)==="." || ( fileData[0] === "" && fileData.length === 2 ) ) {
  	        logger(req, 1, "SS", "Trying to access hidden file. File: "+filepath);
		res.writeHead(404, {'Content-Type': 'text/plain'});
                res.write("Error 404: You're either not allowed to view this page or this page does not exist.");
                res.end();
		return;
	}
	else {
		var fileStream=fs.createReadStream(filepath);

		fileStream.on("end", function() {
			//res.end();
			return;
		});
	
		fileStream.on("error", function(err) {
			logger(req, 1, "SS", "Error reading file: "+filepath+" Err: "+err);
			res.writeHead(500, {"Content-Type": "text/plain"});
			res.write(err);
			res.end();
			return;
		});

		if(ext.indexOf(fileExtension)!==-1) {
			if(ext.indexOf(fileExtension)!==2) {
				//mod to disable caching for non js files, too
				res.writeHead(200, {"Content-Encoding": "gzip", "Content-Type": cType[ext.indexOf(fileExtension)], "Cache-Control": "no-cache"});
			}
			else {
				//header to disable caching for js files
				res.writeHead(200, {"Content-Encoding": "gzip", "Content-Type": cType[ext.indexOf(fileExtension)], "Cache-Control": "no-cache"});
			}
			//use through2 stream for htm/html files only
			if(ext.indexOf(fileExtension)===0 || ext.indexOf(fileExtension)===1) {
				//html files
				//
				//if the user is authenticated, add the user js object to html pages
				if(req.cData && sm.isSessionValid(req)) {
					var user={};
					user["username"] = req.cData.username;
					user["fullname"] = req.cData.fullname;
					user["homepage"] = req.cData.homepage;
					user["mainGroup"] = req.cData.mainGroup;
					user["groups"] = req.cData.groups;
					user["birthday"] = req.cData.birthday;
					user["PhotoURL"] = req.cData.photoURL;

					var userStr = JSON.stringify(user);

					var jsf="<script>\nfunction getLoggedInUser() {\nvar userStr='"+userStr+"';\nvar user=JSON.parse(userStr);\nreturn user;\n}\n</script>\n";

					//var chunkCounter=0;

					var T2 = through2.ctor({"decodeStrings" : false, "encoding": "utf8"},
						function(chunk, enc, next) {
							next(null, chunk);
						},
						function(done) {
							this.push(jsf);
							done();
						});

					var t2 = new T2();

					fileStream
					.pipe(t2)
					.pipe(gzipper)
					.pipe(res);
				}
				//if the user is not authenticated, return the html file without any additions
				else {
					fileStream.pipe(gzipper).pipe(res);
				}
			}
			//end of if for html/htm files --

			//other filetypes
			else {
				fileStream.pipe(gzipper).pipe(res);
			}
		}
		else {
			res.writeHead(200, {"Content-Encoding": "gzip"});

			fileStream.pipe(gzipper).pipe(res);

			fileStream.on("end", function() {
				//res.end();
				return;
			});
		}
	}
};

module.exports = getStaticFile;
