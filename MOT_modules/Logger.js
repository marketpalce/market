var log4js = require("log4js");
var logger = null;
var logLevel = null;
var confFlag = 0;
var ctr = 0;
var logFile = null;
var conf = null;
var logFileSize = null;
var numberOfBackupLogs = null;
var log = {
	init: function(c) {
		conf = c;
		logFile = conf.getConfigParameter("log", "logFile");
		logFileSize = parseInt(conf.getConfigParameter("log", "logFileSize"));
		numberOfBackupLogs = parseInt(conf.getConfigParameter("log", "numberOfBackupLogs"));
	},
	logConfig: function() {
		var logLevels = ["FATAL", "ERROR", "WARN", "INFO", "DEBUG", "TRACE"];
		if (confFlag == 0) {
			conf = require("./ConfigLoader.js");
			logFile = conf.getConfigParameter("log", "logFile");
			logLevel = parseInt(conf.getConfigParameter("log", "logLevel"));
			logFileSize = parseInt(conf.getConfigParameter("log", "logFileSize"));
			numberOfBackupLogs = parseInt(conf.getConfigParameter("log", "numberOfBackupLogs"));
			if (logFile == null || logFile == undefined) {
				logFile = 'log.txt';
				logLevel = 5;
				confFlag = 1;
				console.log("Can't read conf file, will be logging to console and to the default log file: MoT/log.txt. Log level will be set to 5(trace).");
				log4js.configure({
					appenders: [{
						"type": 'console'
					}, {
						"type": 'file',
						"filename": logFile,
						"maxLogSize": logFileSize * 1024 * 1024,
						"backups": numberOfBackupLogs
					}]
				});
				logger = log4js.getLogger();
				logger.setLevel(logLevels[logLevel]);
				return;
			}
			if (logFile && conf.getConfigParameter("log", "logToConsole").toLowerCase() == "true") {
				log4js.configure({
					appenders: [{
						type: 'console'
					}, {
						"type": 'file',
						"filename": logFile,
						"maxLogSize": logFileSize * 1024 * 1024,
						"backups": numberOfBackupLogs
					}]
				});
			} else if (logFile && conf.getConfigParameter("log", "logToConsole").toLowerCase() == "false") {
				log4js.configure({
					appenders: [{
						"type": 'file',
						"filename": logFile,
						"maxLogSize": logFileSize * 1024 * 1024,
						"backups": numberOfBackupLogs
					}]
				});
			}
			logger = log4js.getLogger();
			logger.setLevel();
			confFlag = 1;
			logger.info("Initiated logger with the following settings:\n\tlog file: " + logFile + "\n\tlog level: " + logLevel + "\n\tlog file size: " + logFileSize + "\n\tnumber of backup log files to keep: " + numberOfBackupLogs)
			return;
		} else
			return;
	},
	logMaker: function(req, level, methodName, msg) {
		//log4s config
		if (confFlag == 0) {
			log.logConfig();
		}
		if (level <= logLevel && level >= 0) {
			var ip;
			if (req) {
				try {
					ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress || req.socket.remoteAddress || req.connection.socket.remoteAddress || "IP N/A";
				} catch (err) {
					console.log(">>Error reading sender IP in Logger.");
					ip = "Unknown IP";
				}
			} else
				ip = "SS";
			var logData = "" + ip + " - " + methodName + " - " + msg;
			//console.log("level: "+level+ "\tt.ll: "+logLevel);
			//console.log("logD: "+logData);
			switch (level) {
				case 0:
					console.log("FATAL ERROR: " + logData);
					logger.fatal(logData);
					break;
				case 1:
					logger.error(logData);
					break;
				case 2:
					logger.warn(logData);
					break;
				case 3:
					logger.info(logData);
					break;
				case 4:
					logger.debug(logData);
					break;
				case 5:
					logger.trace(logData);
					break;
			}
		} else {
			return;
		}
	},
};
///
module.exports = log;