var sessions={};

var conf = require("./ConfigLoader.js");

var MAX_SESSIONS=conf.getConfigParameter("sessionManager","maxSessions"); //max sessions for all users

var MAX_DEV_SESSIONS=conf.getConfigParameter("sessionManager","maxDevSessions");; //max dev sessions --> should be stored securely --> licensing dependent

var sessionAge=conf.getConfigParameter("sessionManager", "sessionAge"); //

var logger = require("./Logger").logMaker; //logger(req,level,methodName,msg)

//

var total_sessions=0;

var total_dev_sessions=0;

//

var id=0;


var sessionManagement = {
	add: function(req) {

		sessionData=req.cData;

		if(this.isSessionValid(req)) {
			logger(req, 5, "sessionManagement.add", "User: "+req.cData.username+"@"+req.connection.remoteAddress+" is already logged in with a valid session and tried to login again.");
			return 2;
		}

		if(total_sessions<MAX_SESSIONS) {
			
			sessions[parseInt(++id)]=sessionData;
			sessions[id].sessionId=id;
			
			req.cData.sessionId=id;

			//modification to add username to session variables
			sessions[id].sVars=[];
			
			var un={};
			un["username"]=sessionData.username;
			
			sessions[id].sVars.push(un);
			//end of mod
			
			if(sessionData.userType==2){
				if(total_dev_sessions<MAX_DEV_SESSIONS){
					total_dev_sessions++;
					logger(null,3,"sessionManagement.add","Developer: "+sessionData.username+ " logged on. Total number of developer sessions is: "+total_dev_sessions);
				}
				else{
					return null;
				}
			}

			total_sessions++;
			logger(null,3,"sessionManagement.add","User: "+sessionData.username+ " logged in. Total Number of sessions is: "+total_sessions);
			return 1;
		}
		else{
			return null;
		}
	},
	
	update: function(req) {
		sessionData=req.cData;

		//logger(req, 5, "SessionManager/Update", "Updating user's session data with new timestamp and expiry date.");
		if(req.cData.sessionId) {
			if(this.isSessionValid(req)) {
				sessions[sessionData.sessionId]["timestamp"]=sessionData.timestamp;
				sessions[sessionData.sessionId]["expiryDate"]=sessionData.expiryDate;
			}
		}
	},

	remove: function(req) {

		var sessionId=null;

		if(req.cData.sessionId) {
			sessionId=req.cData.sessionId;
		}

		else {
			return null;
		}

		if(sessions[sessionId] !== null) {
			
			if(sessions[sessionId].userType==2){
				total_dev_sessions--;
			}
			var tempun=sessions[sessionId].username;
			delete sessions[sessionId];
			total_sessions--;
			logger(null,3,"sessionManagement.remove","User: "+tempun+ " logged off or their session was terminated. Total Number of sessions is: "+total_sessions);	
			req.cData={};
			logger(null, 5, "sessionManagement.remove", "Set the value of req.cData for user: "+tempun+" to null");
		} 
		else {
			return null;
		}
	},
	
	getSessionById: function(sessionId) {
		//console.log("\n\nsessions[sessionId]: "+JSON.stringify(sessions[sessionId]));
		
		if(sessions[sessionId])
			return sessions[sessionId];
		else
			return null;
		
	},
		
	addSessionVariable: function(req, vname, vval){
		
		var svar={};
		var sessionId=null;
		svar[vname]=vval;
		
		//console.log("in sm.addsvar, with params sid, vname, vval: "+sessionId+"\t"+vname+"\t"+vval);
		if(this.isSessionValid(req)) {

			sessionId=req.cData.sessionId;
			
			if(sessions[sessionId]["sVars"]){
				//console.log("in second if, svars array exists, and var will be added to it");
				sessions[sessionId]["sVars"].push(svar);			
			}
			else{
				//console.log("in else of second if, svar array doesn't exits, it will created and var will be added");
				sessions[sessionId]["sVars"]=[];
				
				sessions[sessionId]["sVars"].push(svar);
			}
			return 1;
		}
		else{
			return null;
		}
	},
		
	removeSessionVariable: function(req, vname){

		var sessionId=req.cData.sessionId;

		if(this.sessionVariableExists(sessionId, vname)){
			delete sessions[sessionId].sVars[vname];
			return 1;
		}
		else
			return null;
	},
	
	sessionVariableExists: function(req, vname) {
			
			var sessionId=null;

			if(this.isSessionValid(req)) {
				
				sessionId = req.cData.sessionId;

				if(sessions[sessionId].sVars) {
					//console.log("in if .svars");
					//console.log("svar arr: "+JSON.stringify(sessions[sessionId].sVar));
					
					for(var i in (sessions[sessionId].sVars)){
						if(sessions[sessionId].sVars[i][vname])
							return 1;
							//console.log("svi: "+JSON.stringify(sessions[sessionId].sVars[i][vname]));
					}
					/*if(sessions[sessionId].sVars[vname]!="undefined" && sessions[sessionId].sVars[vname]!=null){
						console.log("\t\trequest var exists, 1 will be returned, var: "+sessions[sessionId]["sVars"][vname]);
						return 1;
					}*/
				}
				else{
					//console.log("in svexits 1st else");
					return 0;
				}
			}
			else{
				//console.log("in svexits 2nd else");
				return 0;
			}
	},
	
	getSessionVariable: function(req, vname){

		var sessionId = req.cData.sessionId;

		if(this.sessionVariableExists(req, vname)){
			for(var i in sessions[sessionId].sVars){
				if(sessions[sessionId].sVars[i][vname]){
					return sessions[sessionId]["sVars"][i][vname];
				}
			}
		}
		else
			return null;
	},	
	
	showAll: function(){
		console.log("sessions: \n");
		console.dir(sessions);
	},
		
	isSessionValid: function(req) {
		var sessionId=null;
		var ip = null;
		var sessionExists=0;

		if(req.connection.remoteAddress!==null && typeof(req.connection.remoteAddress)!=='undefined') {
			ip = req.connection.remoteAddress;
		}
		else {
			logger(req, 1, "SessionManager/IsSessionValid", "The request's IP can't be read. The request can't proceed without a known IP.");
			req.cData.sessionValid=0;
			return 0;
		}

		if(typeof(req.cData.sessionId)!=='undefined' && req.cData.sessionId!==null) {
			sessionId = req.cData.sessionId;
		}
		else {
			//logger(req, 5, "SessionManager/IsSessionValid", "The request has no session Id.");
			req.cData.sessionValid=0;
			return 0;
		}

		///
		if(sessions[sessionId]!==null && typeof(sessions[sessionId])!=='undefined') {
			//logger(req, 5, "SessionManager/IsSessionValid", "A session with the id: "+sessionId+" exists. It will now be checked against the request's username and IP.");
			if(sessions[sessionId].username===req.cData.username && sessions[sessionId].ip===ip) {
				//logger(req, 5, "SessionManager/IsSessionValid", "The session: "+JSON.stringify(sessions[sessionId])+" exists and is valid.");
				sessionExists = 1;
			}
			else {
				logger(req, 1, "SessionManager/IsSessionValid", "The request's username and/or IP do not match those stored in the session: "+JSON.stringify(sessions[sessionId])+". The request will not go through.");
				sessionExists = 0;
			}
		}
		else {
			logger(req, 3, "SessionManager/IsSessionValid", "The session does not exist in memory, it might have been terminated, or the server might have been restarted.");
			req.cData.sessionValid=0;
			return 0;
		}
		///
		
		if(sessionExists===1) {
			var currentTime=new Date().getTime();
			if(currentTime < sessions[sessionId].expiryDate) { //check for session expiration
				if(sessions[sessionId].ip===ip)	{ //check source ip against the stored ip
					req.cData.sessionValid=1;
					sessions[sessionId].sessionValid=1;
					//logger(req, 5, "SessionManager/IsSessionValid", "The session: "+JSON.stringify(sessions[sessionId])+" is valid and has not expired, and the requesting IP is the same as the one stored on the server.");
					return 1;
				}
				else { //different ip
					//logger(req, 5, "SessionManager/IsSessionValid", "Although the session exists, and has not expired, the IP in the request does not match the one stored on the server. This request is not valid.");
					req.cData.sessionValid=0;
					return 0;
				}
			}
			else { //session has expired
				//logger(req, 5, "SessionManager/IsSessionValid", "The session: "+JSON.stringify(sessions[sessionId])+" has expired.");
				req.cData.sessionValid=0;
				return 0;
			}
		}
		else {
			//logger(req, 5, "SessionManager/IsSessionValid", "The request refers to a session that does not exist on the server.");
			req.cData.sessionValid=0;
			return 0;
		}
	},
	
	cleanup: function(){
		logger(null, 3, "sessionManagement.cleanup", "Maximum idle session age is: "+sessionAge+" minutes");
		logger(null, 3, "sessionManagement.cleanup", "Cleaning up idle sessions, total number of sessions before cleanup: "+total_sessions);


		for ( var s in sessions) {
			var currentTime=new Date().getTime();
			var sessionExpiryTime=parseFloat(sessions[s]["expiryDate"]);

			if(sessionExpiryTime<currentTime) {
				//delete(sessions[s]);
				//this.remove(sessions[s].sessionId);
				
				if(sessions[s].userType==2) {
					total_dev_sessions--;
				}

				var tempun=sessions[s].username;
				delete sessions[s];
				total_sessions--;

				logger(null,5,"sessionManagement.cleanup","User: "+tempun+ "'s session was terminated. Total Number of sessions is: "+total_sessions);	

				//logger(null, 3, "sessionManagement.cleanup", "Removed 1 session in cleanup");
			}
		}
		logger(null,3,"sessionManagement.cleanup","Cleaned up idle sessions, total number of sessions after cleanup: "+total_sessions);
	}
};

module.exports = sessionManagement;
