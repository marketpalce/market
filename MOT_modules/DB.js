var mysql = require("mysql");

var conf = require("./ConfigLoader.js");

var logger = require("./Logger.js").logMaker; //logger(req,level,methodName,msg)

var pool = null;

var max_connections=parseInt(conf.getConfigParameter("database", "maxConnections"));


var pool_config={
  connectionLimit : max_connections,
  host : conf.getConfigParameter("database","dbip"),
  user : conf.getConfigParameter("database","dbuser"),
  password : conf.getConfigParameter("database","dbpass"),
  database : conf.getConfigParameter("database","dbname"),
	timezone : conf.getConfigParameter("database", "timezone")
};


var DB = {

  createConnection : function DBcreateConnectionFn() {

  	pool = mysql.createPool(pool_config);
    
    pool.on('error', function(err) {
      logger(null, 1, "DB", "Err: "+err);
      if(err.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the MySQL server is usually
        handleDisconnect();                         // lost due to either server restart, or a
      } 
      else {                                      // connnection idle timeout (the wait_timeout
        throw err;                                  // server variable configures this)
      }
    });
    logger(null, 3, "DB", "Connected to database successfully");
    logger(null, 3, "DB", "Maximum size of DB connection pool is: "+max_connections);
  }(),

  dbConn : function dbConnFn() {
  	if(pool==null){
  		this.createConnection();
  	}
  	return pool;
  },

  handleDisconnect : function DBhandleDisconnectFn() {
    logger(null, 3, "ConnectToDatabase", "In handleDisconnect()");

    pool = mysql.createPool(pool_config); // Recreate the connection, since the old one cannot be reused.
    pool.connect(function(err) {              // The server is either down
      if(err) {                                     // or restarting (takes a while sometimes).
        logger(null, 1, "ConnectToDatabase/handleDisconnect()","Error when connecting to db after disconnect: "+err);
        setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
      }                                     // to avoid a hot loop, and to allow our node script to
    });                                     // process asynchronous requests in the meantime.
                                            // If you're also serving http, display a 503 error.
    pool.on('error', function(err) {
      logger(null, 1, "ConnectToDatabase/handleDisconnect()", "Err: "+err);
      if(err.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the MySQL server is usually
        handleDisconnect();                         // lost due to either server restart, or a
      } 
      else {                                      // connnection idle timeout (the wait_timeout
        throw err;                                  // server variable configures this)
      }
    });
  },

  endConnection : function DBendConnectionFn() {
    if(pool!=null) {
      pool.end(function(err) {
        if(err){
          logger(null, 1, "ConnectToDatabase/endConnection()", "There was an error when terminating the connection. Err: "+err);
        }
        else{
          logger(null, 3, "ConnectToDatabase/endConnection()", "DB connection terminated successfully.");
        }
      });
    }
  }
};

//module.exports.dbConn = dbConn;
//module.exports.endConnection = endConnection;
module.exports = DB;
