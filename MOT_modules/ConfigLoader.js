var xmlmapping = require("xml-mapping");
var fs = require("fs");
var confFile=null;
var logger=null;

var conf = {

	//logger: null,
	//confFile : null,

	init : function(log) {
		logger = log.logMaker;

		var temp=fs.readFileSync("config.xml");

		confFile=xmlmapping.load(temp.toString(), {throwErrors: true});	
		
		logger(null, 3, "ConfigLoader", "Loaded configuration file: config.xml");
		logger(null, 5, "ConfigLoader", "Loaded configuration file : config.xml :: "+JSON.stringify(confFile));
	},

	getConfigParameter : function(section, parameter) {
		if(confFile) {
			if(confFile.MOT[section][parameter]){
				//logger(null, 5, "ConfigLoader-GetConfigParameter", "Requested config parameter: "+section+"/"+parameter+" = "+confFile.MOT[section][parameter].$t);
				return confFile.MOT[section][parameter].$t;
			}
			else {
				logger(null, 1, "ConfigLoader-GetConfigParameter", "Requested config parameter: "+section+"/"+parameter+" does not exist.");
				return null;
			}
		}
		else {
			logger(null, 0, "ConfigLoader-GetConfigParameter", "Can't read the configuration file.");
			console.log("ConfigLoader: can't read conf file");
			return null;
		}
	},

};

module.exports.getConfigParameter=conf.getConfigParameter;
module.exports = conf;
