exports.services = {
/*	CreateNewProject: {
		"service": require("./ide/CreateNewProject_srv.js"),
		"access": [2]
	},
	RemoveProject: {
		"service": require("./ide/RemoveProject_srv.js"),
		"access": [2]
	},
	SaveProject: {
		"service": require("./ide/SaveProject_srv.js"),
		"access": [2]
	}

       countries: {"service": require("./services/countries_service")},
       regions: require("./services/regions_service"),
       cities: require("./services/cities_service"),*/
       users: {
		"service": require("./services/users_service"),
		"access": [3]
	},
       register: {
		"service": require("./services/users_service"),
		"access": [-1]
	},
       login: {
		"service": require("./services/login_service"),
		"access": [-1]
	},
       categories: {
		"service": require("./services/categories_service"),
		"access": [-1]
	}

};
