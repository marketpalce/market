/***core modules***/
var qs = require("querystring");
var path = require("path");
/***core modules***/

///

/***installed modules***/
var bcrypt = require("bcrypt");
/***installed modules***/

///

/***our utilities***/
var sm = require("./../MOT_modules/sessionManager.js");
var connection=require('./../MOT_modules/DB.js').dbConn();
var conf=require("./../MOT_modules/ConfigLoader.js");
//shortcut to logMaker to avoid calling log.logMaker
//adjust path of logger.js file according to location of current file
var logger = require("./../MOT_modules/Logger.js").logMaker; //logger(req,level,methodName,msg)
/***our utilities***/

///

/***global variables***/
var sessionAge=parseInt(conf.getConfigParameter("sessionManager","sessionAge"));

var uTableName="`users`";
/***global variables***/

///

var Login = function LoginFn(req, res) {
	
	var requestBody='';
	var parsedBody='';
	var username;
	var password;


	req.on("data", function(data) {
		requestBody+=data;
	});

	req.on("error", function(err) {
		res.writeHead(500);
		res.end(err.toString());
		return;
	});

	req.on("end", function() {

		parsedBody=qs.parse(requestBody);


		if(parsedBody["username"] && parsedBody["password"]) {
			username = parsedBody["username"];
			password = parsedBody["password"];

			logger(req, 5, "Login_srv", "User: "+username+" trying to login.");
		}

		else if(parsedBody["username"]) {
			username = parsedBody["username"];

			if(username==="Guest") {
				logger(req, 5, "Login_srv", "A user from "+req.connection.remoteAddress+" is trying to login as guest.");

				//req.cData={};

				setUserData(req, "Guest", null);

				/*if(!sm.isSessionValid(req)) {
					sm.add(req);
				}*/
				if(sm.add(req)==2) {
					logger(req, 5, "Login_srv", "A user from "+req.connection.remoteAddress+" is trying to login as guest, but they're already logged in as guest");
				}

				else {
					logger(req, 5, "Login_srv", "User: "+req.cData.username+"@"+req.connection.remoteAddress+" is already logged in, and was trying to login again.");
				}

				redirectUser(req, res);
				return;
			}

			else {
				logger(req, 1, "Login_srv", "User: "+req.cData.username+"@"+req.connection.remoteAddress+" is trying to login without entering a password.");
				res.statusCode=404;
				res.end();
				return;
			}
		}

		else {
			logger(req, 1, "Login_srv", "The user did not enter their username and/or password.");
			res.statusCode = 404;
			res.writeHead(404);
			res.end();
			return;
		}
		
		var query="select `id`, `password`, `role` from "+uTableName+" where `username` = '"+username+"'";
	

		//user is not a guest
		
		connection.query(query, function loginQueryCB(err, results) {
			if(err) {
				logger(req, 1, "Login_srv", "Error getting user data from DB.");
				logger(req,4,"Login_srv","Error getting user data from DB. Query: "+query+"\tErr: "+err);
				var responseText = "This username does not exist.";
				res.writeHead(500);
				res.end(responseText);
				return;
			}

			if(results[0]) {
				bcrypt.compare(password, results[0]["password"], function(err, result) {			
					if(err) {
						logger(req, 1, "Login", "Error comparing password hash. Err: "+err);
						res.writeHead(500, {'Content-Type': 'plain-text'});
						res.end(err.toString());
						return;
					}
					//valid username and password
					else if(result) {
						req.cData={};

						setUserData(req, username, username, results);

						if(req.cData.loginError)
							req.cData.loginError=null;
						
						if(!sm.isSessionValid(req)) {
							sm.add(req);
						}
						else {
							logger(req, 5, "Login_srv", "User: "+req.cData.username+"@"+req.connection.remoteAddress+" is already logged in, and was trying to login again.");

							//the code after this conditional does not to be applied if the user is already logged in
							redirectUser(req, res);
							return;
						}


						redirectUser(req, res);

					}

					//incorrect username and password combination
					else {
						//res.writeHead(404, {'Content-Type': 'text/html' });
						//res.write("<p>incorrect un &/or pw</p>");
						
						if(req.cData.loginError)
							req.cData.loginError+=1;
						else
							req.cData.loginError=1;

						//this should cause the display of an error in the login page somehow!!
						res.writeHead(301, { Location: (req.socket.encrypted ? 'https://' : 'http://') + req.headers.host + "/login.html?loginError=" + req.cData.loginError, "Cache-Control": "no-cache"} );
						res.end();
					}
				});	
			}

			//else for if(results[0]) -- this means that there was an error with getting the data from the DB, but it was not 
			//a DB error -- I don't know if this is even a valid case!!

			else {
				logger(req, 1, "Login_srv", "Invalid username: " + username );
				
				if(req.cData.loginError)
					req.cData.loginError+=1;
				else
					req.cData.loginError=1;

				//this should cause the display of an error in the login page somehow!!
				res.writeHead(301, { Location: (req.socket.encrypted ? 'https://' : 'http://') + req.headers.host + "/login.html?loginError=" + req.cData.loginError, "Cache-Control": "no-cache"} );
				res.end();
			}

		});
		//end of else -- user is not a guest

	});
};


var setUserData = function setUserDataFn(req, username, name, results) {
	
	var date=new Date();
	var date2=new Date();
	date2.setMinutes(date2.getMinutes()+sessionAge);

	req.cData.timestamp= date.getTime();
	req.cData.expiryDate=date2.getTime();
	req.cData.fullname=name;
	req.cData.username=username;
	req.cData.ip=req.connection.remoteAddress;
	req.cData.homepage="/";

	if(results!==null) {
		req.cData.userId=results[0]["id"];
		req.cData.userType=results[0]["role"];
	}
	else {
		req.cData.userId=5;
		req.cData.userType=5;
	}

	logger(req, 5, "Login/SetUserData", "User logged in, cookie data set to: "+JSON.stringify(req.cData));
	return;
};


var redirectUser = function redirectUserFn(req, res) {

	console.log("in redir user after login");

	if(typeof req.cData.requestedLoc !== 'undefined' && req.cData.requestedLoc !== null) {


		console.log("user requested loc: "+req.cData.requestedLoc+" and is about to be redirected to it");
		var requestedLocation=req.cData.requestedLoc;
		req.cData.requestedLoc=null;
		res.writeHead(301, { Location: (req.socket.encrypted ? 'https://' : 'http://') + req.headers.host + requestedLocation, "Cache-Control": "no-cache"});
		res.end();
		return;
	}

	else {

		console.log("user didn't request a specific location, and will be redirected to their homepage: "+req.cData.homepage);

		res.writeHead(301, { Location: req.cData.homepage, "Cache-Control": "no-cache"});
		res.end();
		return;
		
	}
};

///
exports.dispatch = {
	GET:    null,
	DELETE: null,
	PUT:    null,
	POST:   Login
};
