/***core modules***/
var qs = require("querystring");
var url = require("url");
/***core modules***/

///

/***installed modules***/
var bcrypt = require("bcrypt");
/***installed modules***/

///

/***our utilities***/
//shortcut to logMaker to avoid calling log.logMaker
var logger = require("./../MOT_modules/Logger.js").logMaker; //logger(req,level,methodName,msg)

var connection=require("./../MOT_modules/DB.js").dbConn();
/***our utilities***/


/***global variables***/
var uTableName = "`users`";
/***global variables***/


//


var CreateUser = function CreateUserFn(req, res) {

	var requestBody='';

	req.on("data", function(chunk) {

		requestBody+=chunk;
	});

	req.on("error", function(err) {
		logger(req, 1, "Group/CreateUser", "An error occurred when the server was receiving a request from the client.");
		res.writeHead(400);
		res.end();
		return;
	});

	req.on("end", function() {

		var parsedBody;

		//console.log("typeof rb: "+typeof requestBody);
		//console.log("user req body: "+JSON.stringify(requestBody));

		if(typeof requestBody=='string') {
			console.log("pb == str")

			if(requestBody.indexOf("{")!==-1) {
				try {
					parsedBody=JSON.parse(requestBody);
				}
				catch(err) {
					console.log("Error parsing req json in User. Err: "+err);
				}
			}
			else {
				console.log("====\nUser rb: ");
				console.dir(requestBody);
				console.log("=====\n");

				parsedBody=qs.parse(requestBody);

				console.log("====\nUser pb: ");
				console.dir(parsedBody);
				console.log("=====\n");
			}
		}
		else {
			console.log("pb == obj")
			parsedBody=JSON.parse(requestBody);
		}

		if(parsedBody["username"] && parsedBody["password"] && parsedBody["email"] && parsedBody["userType"]) {
			var username = parsedBody["username"];
			var password = parsedBody["password"];
			var repeatPassword = parsedBody["repeatPassword"];
			var email = parsedBody["email"];
			var userType = parsedBody["userType"];

			if(password !== repeatPassword) {
				logger(req, 1, "User/CreateUser","User: "+req.cData.userId+" tried to create a user but the password and its confirmation did not match.");
				res.writeHead(409);
				res.end();
				return;
			}

			//proceed normally since all data was sent and password and repeatPassword match
			bcrypt.genSalt(10, function(err, salt) {
				bcrypt.hash(password, salt, function(err, hash) {
					if(err) {
						logger(req, 1, "User.CreateUser" , "Error hashing password: "+err);
						res.end(err.toString());
						return;
					}
					var userData={"username": username, "Password": hash, "email": email, "role": userType};
					logger(req, 4, "User.CreateUser", "User data from form: "+JSON.stringify(userData));
					var query="insert into " + uTableName + " set ?";
					
					connection.query(query, userData,function(err, result) {
						if(err) {
							if(err.code=="ER_DUP_ENTRY") {
								logger(req, 1, "User/CreateUser", "Error because the admin tried to create a user with a username or email that already exists.");
								logger(req, 5, "User/CreateUser", "Duplicate entry for username and/or email: "+JSON.stringify(userData));
								res.writeHead(409);
								res.end("Duplicate User Name: "+userData["Username"]+" or Email: "+userData["Email"]);
								return;
							}
							else {
								logger(req, 1, "User/CreateUser", "Error inserting user data into DB: "+err);
								logger(req, 5, "User/CreateUser", "Error inserting user data using query: "+query+" Err: "+err);
								res.writeHead(500, {'Content-Type': 'text/html' });
								res.end("DB Error.");
								return;
							}
						}
						else{
							res.writeHead(303, { Location: "/"});
							res.end();
							return;
						}
						
					});	
				});	
			});
		}
		else {
			logger(req, 1, "User/CreateUser", "User: "+req.cData.userId+" tried to create a user but the data sent was incomplete.");
			logger(req, 1, "User/CreateUser", "Incomplete data sent when trying to create user. !!!Data: "+JSON.stringify(parsedBody));
			res.writeHead(400);
			res.end("Parameters incomplete. Parameters: "+JSON.stringify(parsedBody));
			return;
		}
	});
};

///


//this function is used to change user data,
/*

should we allow a user to change his username or not?

*/

var UpdateUser = function UpdateUserFn(req, res) {
	var requestBody='';

	req.on("data", function(data) {
		requestBody+=data;
	});

	req.on("error", function(err) {
		logger(req, 1, "Group/UpdateUser", "An error occurred when the server was receiving a request from the client.");
		res.writeHead(400);
		res.end();
	});

	req.on("end", function() {

		//var parsedBody=qs.parse(requestBody);

		/*var userId = parsedBody["userId"];
		var username = parsedBody["username"];
		var password = parsedBody["password"];
		var fname = parsedBody["fname"];
		var lname = parsedBody["lname"];
		var email = parsedBody["email"];
		var userType = parsedBody["userType"];*/

		var urlParts = url.parse(req.url,true);

		var urlData = urlParts.query;

		if(urlData["id"] && urlData["email"] && urlData["userType"]) {

			var userData={};

			var id = urlData["id"];

			userData["Email"] = urlData["email"];
			userData["UserType"] = urlData["userType"];

			if(urlData["password"] && urlData["repeatPassword"]) {
				userData["Password"]=urlData["password"];

				if(urlData["password"]!==urlData["repeatPassword"]) {
					logger(req, 1, "User/UpdateUser","User: "+req.cData.userId+" tried to update a user but the password and its confirmation did not match.");
					res.writeHead(409);
					res.end();
					return;
				}
			}

			logger(req, 5, "User/UpdateUser", "UpdateUser received the following data: "+JSON.stringify(userData));
		

			if(typeof userData["Password"]==='undefined' || userData["Password"]===null || userData["Password"]==="") {
				logger(req, 5, "UpdateUser", "Updating user data for: "+userData["Username"]+" without changing the password.");
				setUserData(req, res, id, userData, null);
			}

			else {
				logger(req, 5, "UpdateUser", "Updating user data for: "+userData["Username"]+" and changing their password.");
				setUserData(req, res, id, userData, userData["Password"]);
			}
		}
		//this else is entered when the data sent is erroneous	
		else {
			logger(req, 1, "User/UpdateUser", "User: "+req.cData.userId+" tried to update a user but the data sent was incomplete.");
			logger(req, 1, "User/UpdateUser", "Incomplete data sent when trying to update a user. Data: "+JSON.stringify(urlData));
			res.writeHead(409);
			res.end();
			return;
		}
	});
};

///

var DeleteUser = function DeleteUserFn(req, res) {
	var requestBody='';

	req.on("data", function(data){
		requestBody+=data;
	});

	req.on("error", function(err) {
		logger(req, 1, "Group/DeleteUser", "An error occurred when the server was receiving a request from the client.");
		res.writeHead(400);
		res.end();
	});

	req.on("end", function(){

		var parsedBody=qs.parse(requestBody);

		//var id = parseInt(parsedBody["id"]);

		var urlParts = url.parse(req.url,true);

		var id = parseInt(urlParts.query["id"]);

		var deleteUserQuery = "update "+uTableName+" set role=1 where `id`="+id;

		connection.query(deleteUserQuery, function deleteUserQueryCB(err, result) {
			if(err) {
				logger(req, 1, "User.DeleteUser" , "Error deleting user with id: "+id+"\tErr: "+err);
				logger(req, 5, "User.DeleteUser", "Error deleting user with id: "+id+"\tErr: "+err+"\tQuery: "+deleteUserQuery);
				res.end(err.toString());
				return;
			}

			else {
				res.writeHead(303, { Location: "admin/userAdmin.html"});
				res.end();
				return;
			}
		});
	});
};

///

var GetUserData = function GetUserDataFn(req, res) {
	var urlParts = url.parse(req.url,true);

	logger(req, 5, "User/GetUserData", "Request to User/GetUserData with parameters: "+JSON.stringify(urlParts.query));


	var id;
	var username;
	var getUserDataQuery;


	if(urlParts.query["id"]) {
		id = urlParts.query["id"];
		getUserDataQuery = "select `id`, `username`, `role`, `email` from "+uTableName+" where `id`="+id;
	}
	else if(urlParts.query["username"]){
		username=urlParts.query["username"];
		getUserDataQuery = "select `id`, `username`, `role`, `email` from "+uTableName+" where `username`='"+username+"'";
	}
	else {
		logger(req, 1, "User/GetUserData", "Error getting user data because the required parameters were not sent or were not sent in the proper format.");
		logger(req, 5, "User/GetUserData", "Error getting user data. Parameters sent to GetUserData: "+JSON.stringify(urlParts.query));
		res.writeHead(409);
		res.end();
		return;
	}


	connection.query(getUserDataQuery, function getUserDataQueryCB(err, result) {
		if(err) {
			logger(req, 1, "User.GetUserData" , "Error getting user data from DB. Err "+err);
			logger(req, 5, "User.GetUserData", "Error getting user data form DB. Err: "+err+"\tQuery: "+getUserDataQuery);
			res.end(err);
			return;
		}

		else {

			var userId;

			if(result[0] && result[0]["id"]!==null && typeof result[0]["id"]!==undefined)
				userId=result[0]["id"];
			else {
				logger(req, 1, "User/GetUserData", "Requesting user data for non-existing user or using invalid parameters.");
				logger(req, 5, "User/GetUserData", "Requesting user data for non-existing user or using invalid parameters. DB Results: "+JSON.stringify(result));
				res.writeHead(409);
				res.end();
				return;
			}
			var userData = JSON.stringify(result[0]);
			var UDO=JSON.parse(userData);


			logger(req, 5, "User.GetUserData", "User data:\n"+JSON.stringify(UDO)+"\nprepared for sending in the response.");

			finalUD=JSON.stringify(UDO);

			res.writeHead(200, {'Content-Type': 'application/json'});
			res.end(finalUD);
			return;
		}
	});
};

///


var setUserData = function setUserDataFn(req, res, id, data, password) {

	var userData={};

	logger(req, 5, "User/SetUserData", "User data: "+JSON.stringify(data));

	userData={"email": data["email"], "role": data["UserType"]};

	if(password!==null) {
		bcrypt.genSalt(10, function(err, salt) {
			bcrypt.hash(password, salt, function(err, hash) {
				if(err){
					logger(req, 1, "User.UpdateUser" , "Error hashing password: "+err);
					res.end(err);
					return;
				}
				else {

					userData["password"]=hash;

					insertUserDataIntoDB(req, id, userData, res);
				}
			});	
		});
	}

	else {
		insertUserDataIntoDB(req, id, userData, res);
	}
};
///

var insertUserDataIntoDB = function insertUserDataIntoDBFn(req, id, data, res) {

	logger(req, 4, "User.UpdateUser.InsertUserDataIntoDB", "User data from form: "+JSON.stringify(data));
	
	var query="update " + uTableName + " set ? where id="+id;
	
	connection.query(query, data, function insertUserDataIntoDBUpdateUserCB(err, results){
		if(err){
			if(err.code=="ER_DUP_ENTRY") {
				logger(req, 1, "User/UpdateUser", "Error because the admin tried to update the user's email to one that already exists for a different user.");
				logger(req, 5, "User/UpdateUser", "Erroneous data: "+JSON.stringify(data));
				res.writeHead(409);
				res.end(err.toString());
				return;
			}
			else {
				logger(req, 1, "User/UpdateUser", "Error inserting user data into DB: "+err);
				logger(req, 4, "User/UpdateUser", "Error inserting user data using query: "+query+" Err: "+err);
				res.writeHead(500, {'Content-Type': 'text/html' });
				res.end(err.toString());
				return;
			}
		}
		else {
			res.writeHead(303, { Location: "admin/userAdmin.html"});
			res.end();
			return;
		}
	});	
};
///



///add method names according to their respective request methods below
exports.dispatch = {
	GET:    GetUserData,
	DELETE: DeleteUser,
	PUT:    UpdateUser,
	POST:   CreateUser
};
