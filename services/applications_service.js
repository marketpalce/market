/***core modules***/
var qs = require("querystring");
var url = require("url");
var formidable = require('formidable');
/***core modules***/

///

/***installed modules***/
var bcrypt = require("bcrypt");
/***installed modules***/

///

/***our utilities***/
//shortcut to logMaker to avoid calling log.logMaker
var logger = require("./../MOT_modules/Logger.js").logMaker; //logger(req,level,methodName,msg)

var connection=require("./../MOT_modules/DB.js").dbConn();
/***our utilities***/


/***global variables***/
var appTableName = "`applications`";
/***global variables***/


//


var CreateApp = function CreateAppFn(req, res) {

	var requestBody='';

	req.on("data", function(chunk) {

		requestBody+=chunk;
	});

	req.on("error", function(err) {
		logger(req, 1, "Group/CreateApp", "An error occurred when the server was receiving a request from the client.");
		res.writeHead(400);
		res.end();
		return;
	});

	req.on("end", function() {


		//console.log("typeof rb: "+typeof requestBody);
		//console.log("user req body: "+JSON.stringify(requestBody));

		if(typeof requestBody=='string') {
			console.log("pb == str")
            console.log(requestBody);

			if(requestBody.indexOf("{")!==-1) {
				try {
					parsedBody=JSON.parse(requestBody);
				}
				catch(err) {
					console.log("Error parsing req json in User. Err: "+err);
				}
			}
			else {
				console.log("====\nUser rb: ");
				console.dir(requestBody);
				console.log("=====\n");

				parsedBody=qs.parse(requestBody);
				console.log("====\nUser pb: ");
				console.dir(parsedBody);
				console.log("=====\n");
			}
		}
		else {
			console.log("pb == obj")
			parsedBody=JSON.parse(requestBody);
		}
        console.log("Parsed body is : "+JSON.stringify(parsedBody));
		if(parsedBody["category"] && parsedBody["app_type"] && parsedBody["name"] && parsedBody["version"] && parsedBody["icon"] && parsedBody["prerequisit"] && parsedBody["description"]) {
			var category = parsedBody["category"];
			var app_type = parsedBody["app_type"];
			var name = parsedBody["name"];
			var version = parsedBody["version"];
			var icon = parsedBody["icon"];
            var prerequisit = parsedBody["prerequisit"];
            var description = parsedBody["description"];


			//proceed normally since all data was sent and password and repeatPassword match
					var appData={"category": category, "app_type": app_type, "name": name, "version": version,"icon": icon,"prerequisit": prerequisit,"description": description };
					logger(req, 4, "App.CreateApp", "App data from form: "+JSON.stringify(appData));
					var query="insert into " + appTableName + " set ?";
					
					connection.query(query, appData,function(err, result) {
						if(err) {
							if(err.code=="ER_DUP_ENTRY") {
								logger(req, 1, "Application/CreateApp", "Error because the admin tried to create a app with a name that already exists.");
								logger(req, 5, "Application/CreateApp", "Duplicate entry for name: "+JSON.stringify(appData));
								res.writeHead(409);
								res.end("Duplicate App Name: "+appData["name"]);
								return;
							}
							else {
								logger(req, 1, "Application/CreateApp", "Error inserting app data into DB: "+err);
								logger(req, 5, "Application/CreateApp", "Error inserting app data using query: "+query+" Err: "+err);
								res.writeHead(500, {'Content-Type': 'text/html' });
								res.end("DB Error.");
								return;
							}
						}
						else{
                            var getLastRowQuery="select * from applications order by id desc limit 1";
                            connection.query
							res.writeHead(303, { Location: "/"});
							res.end();
							return;
						}
			});
		}
		else {
			logger(req, 1, "User/CreateUser", "User: "+req.cData.userId+" tried to create a user but the data sent was incomplete.");
			logger(req, 1, "User/CreateUser", "Incomplete data sent when trying to create user. !!!Data: "+JSON.stringify(parsedBody));
			res.writeHead(400);
			res.end("Parameters incomplete. Parameters: "+JSON.stringify(parsedBody));
			return;
		}
	});
};

///


//this function is used to change user data,
/*

should we allow a user to change his username or not?

*/

var UpdateApp = function UpdateAppFn(req, res) {
	var requestBody='';

	req.on("data", function(data) {
		requestBody+=data;
	});

	req.on("error", function(err) {
		logger(req, 1, "Group/UpdateUser", "An error occurred when the server was receiving a request from the client.");
		res.writeHead(400);
		res.end();
	});

	req.on("end", function() {

		//var parsedBody=qs.parse(requestBody);

		/*var userId = parsedBody["userId"];
		var username = parsedBody["username"];
		var password = parsedBody["password"];
		var fname = parsedBody["fname"];
		var lname = parsedBody["lname"];
		var email = parsedBody["email"];
		var userType = parsedBody["userType"];*/

		var urlParts = url.parse(req.url,true);

		var urlData = urlParts.query;

		if(urlData["id"] && urlData["category"] && urlData["app_type"] && urlData["name"] && urlData["version"] && urlData["icon"] && urlData["prerequisit"] && urlData["description"]) {

			var appData={};

			var id = urlData["id"];

			appData["category"] = urlData["category"];
			appData["app_type"] = urlData["app_type"];
            appData["name"] = urlData["name"];
            appData["version"] = urlData["version"];
            appData["icon"] = urlData["icon"];
            appData["prerequisit"] = urlData["prerequisit"];
            appData["description"] = urlData["description"];



			logger(req, 5, "User/UpdateUser", "UpdateUser received the following data: "+JSON.stringify(appData));
		

            setAppData(req,res,id,appData);
		}
		//this else is entered when the data sent is erroneous	
		else {
			logger(req, 1, "User/UpdateUser", "User: "+req.cData.userId+" tried to update a user but the data sent was incomplete.");
			logger(req, 1, "User/UpdateUser", "Incomplete data sent when trying to update a user. Data: "+JSON.stringify(urlData));
			res.writeHead(409);
			res.end();
			return;
		}
	});
};

///

var DeleteApp = function DeleteAppFn(req, res) {
	var requestBody='';

	req.on("data", function(data){
		requestBody+=data;
	});

	req.on("error", function(err) {
		logger(req, 1, "Group/DeleteUser", "An error occurred when the server was receiving a request from the client.");
		res.writeHead(400);
		res.end();
	});

	req.on("end", function(){


		var parsedBody=qs.parse(requestBody);
		//var id = parseInt(parsedBody["id"]);

		var urlParts = url.parse(req.url,true);

		var id = parseInt(urlParts.query["id"]);

		var deleteAppQuery = "delete from "+appTableName+" where `id`="+id;

		connection.query(deleteAppQuery, function deleteAppQueryCB(err, result) {
			if(err) {
				logger(req, 1, "Application.DeleteApp" , "Error deleting app with id: "+id+"\tErr: "+err);
				logger(req, 5, "Application.DeleteApp", "Error deleting app with id: "+id+"\tErr: "+err+"\tQuery: "+deleteAppQuery);
				res.end(err.toString());
				return;
			}

			else {
				res.writeHead(200, { Deleted: "App deleted Successfuly"});
				res.end();
				return;
			}
		});
	});
};

///
function isEmpty(obj) {
    return Object.keys(obj).length === 0;
}

var GetApplications = function GetAppDataFn(req, res) {
	var urlParts = url.parse(req.url,true);

	logger(req, 5, "Applications/GetApplicationData", "Request to Applications/GetApplicationData with parameters: "+JSON.stringify(urlParts.query));


	var id;
    var category;
	var getAppDataQuery;

    console.log(urlParts.query);
	if(urlParts.query["id"]) {
		id = urlParts.query["id"];
		getAppDataQuery = "select `id`, `category`, `app_type`, `name`, `version`, `downloads_no`, `icon`, `prerequisit`, `last_updated`, `description`,`user_id` from "+appTableName+" where `id`="+id;
	}
    else if(urlParts.query["category"]){
        //get all applications
        category = urlParts.query["category"];
        getAppDataQuery = "select * from "+appTableName+" where `category`='"+category+"'";
    }
	else if(isEmpty(urlParts.query)){
	    //get all applications
		getAppDataQuery = "select * from "+appTableName+"";
	}
	else {
		logger(req, 1, "Applications/GetAppData", "Error getting app data because the required parameters were not sent or were not sent in the proper format.");
		logger(req, 5, "Applications/GetApprData", "Error getting app data. Parameters sent to GetAppData: "+JSON.stringify(urlParts.query));
		res.writeHead(409);
		res.end();
		return;
	}


	connection.query(getAppDataQuery, function getAppDataQueryCB(err, result) {
		if(err) {
			logger(req, 1, "App.GetAppData" , "Error getting app data from DB. Err "+err);
			logger(req, 5, "App.GetAppData", "Error getting app data form DB. Err: "+err+"\tQuery: "+getAppDataQuery);
			res.end(err);
			return;
		}

		else {

			var appId;
            var UDO=[];
            for(var i=0;i<result.length;i++) {
                if (result[i] && result[i]["id"] !== null && typeof result[i]["id"] !== undefined)
                    appId = result[i]["id"];
                else {
                    logger(req, 1, "Application/GetAppData", "Requesting user data for non-existing user or using invalid parameters.");
                    logger(req, 5, "Applicaiton/GetAppData", "Requesting user data for non-existing user or using invalid parameters. DB Results: " + JSON.stringify(result));
                    res.writeHead(409);
                    res.end();
                    return;
                }
                var appData = JSON.stringify(result[i]);
                UDO.push(JSON.parse(appData));


                logger(req, 5, "User.GetAppData", "User data:\n" + JSON.stringify(UDO) + "\nprepared for sending in the response.");

            }
            finalUD=JSON.stringify(UDO);
			res.writeHead(200, {'Content-Type': 'application/json'});
			res.end(finalUD);
			return;
		}
	});
};

///


var setAppData = function setAppDataFn(req, res, id, data) {

	var appData={};

	logger(req, 5, "Application/SetAppData", "App data: "+JSON.stringify(data));

	appData={"category": data["category"], "app_type": data["app_type"], "name": data["name"], "version": data["version"], "icon": data["icon"], "prerequisit": data["prereqisit"], "description": data["description"]};

    insertAppDataIntoDB(req,id,appData,res);
};
///

var insertAppDataIntoDB = function insertAppDataIntoDBFn(req, id, data, res) {

	logger(req, 4, "User.UpdateUser.InsertUserDataIntoDB", "User data from form: "+JSON.stringify(data));
	
	var query="update " + appTableName + " set ? where id="+id;
	
	connection.query(query, data, function insertAppDataIntoDBUpdateAppCB(err, results){
		if(err){
			if(err.code=="ER_DUP_ENTRY") {
				logger(req, 1, "User/UpdateUser", "Error because the admin tried to update the user's email to one that already exists for a different user.");
				logger(req, 5, "User/UpdateUser", "Erroneous data: "+JSON.stringify(data));
				res.writeHead(409);
				res.end(err.toString());
				return;
			}
			else {
				logger(req, 1, "User/UpdateUser", "Error inserting user data into DB: "+err);
				logger(req, 4, "User/UpdateUser", "Error inserting user data using query: "+query+" Err: "+err);
				res.writeHead(500, {'Content-Type': 'text/html' });
				res.end(err.toString());
				return;
			}
		}
		else {
			res.writeHead(303, { Location: "admin/userAdmin.html"});
			res.end();
			return;
		}
	});	
};
///



///add method names according to their respective request methods below
exports.dispatch = {
	GET:    GetApplications,
	DELETE: DeleteApp,
	PUT:    UpdateApp,
	POST:   CreateApp
};
