
/***our utilities***/
//shortcut to logMaker to avoid calling log.logMaker
var logger = require("./../MOT_modules/Logger.js").logMaker; //logger(req,level,methodName,msg)

var connection=require("./../MOT_modules/DB.js").dbConn();
/***our utilities***/


/***global variables***/
var appTableName = "`categories`";
/***global variables***/


var getCategories = function(params, callback) {
    //var countryNameLike = aux.getParameter(params, "countrynamelike");

    return aux.conn.query("SELECT * FROM categories ORDER BY category",
        function (err, rows) {
            if (err)
                return aux.onError(err, callback);

            var pending = rows.length;
            if (pending == 0)
                return callback(404, "NOT FOUND");

            var result = [];
            for (var i in rows) {
                result[i] = {
                    category: {
                               name: rows[i]["category"],
                               id: rows[i]["id"]
                              }
                };
            }
        //    console.log("ana hnaaa");
            return callback(200, "Found",{},result);
        });
};

var postCategories = function (params, callback) {
    //var categoryCode = aux.getParameter(params,"id");
    var categoryName = aux.getParameter(params, "category");

    if (!categoryName && params._method==="POST")
        return callback(405, "MUST SPECIFY Category Name", {
            "Allowed": "GET, PUT, DELETE"
        });

    //if (!categoryName)
    //    return callback(403, "MUST SPECIFY Category NAME");

    return aux.conn.query("INSERT INTO categories SET ?",
        {
            "category":categoryName
        },
        function (err) {
            if (err)
                return aux.onError(err, callback);

            return callback(201, "CREATED", {"mycategory":categoryName});
        });
};

var putCategories = function (params, callback) {
    var categoryid = aux.getParameter(params,"id");
    var categoryName = aux.getParameter(params, "category");

    if (!categoryid && !categoryName && params._method==="POST")
        return callback(405, "Can't Update,Missing Argument", {
            "Allowed": "GET, PUT, DELETE"
        });

    //if (!categoryName)
    //    return callback(403, "MUST SPECIFY Category NAME");

    return aux.conn.query("UPDATE categories SET ? WHERE "
        + aux.isEqual("id", categoryid),
        {
            "category":categoryName
        },
        function (err,result) {
            if (err)
                return aux.onError(err, callback);

            if (result.affectedRows)
                return callback(204, "UPDATED", {"update": "Category Updated"});

            else
                return callback(409, "COULDN'T UPDATE Category");
        });
};


var deleteCategories = function(params, callback) {
    var categoryId = aux.getParameter(params, "id");
   console.log("ID"+categoryId);
    if (!(categoryId))
        return callback(400, "MUST SPECIFY A CATEGORY; CANNOT DELETE ALL");

    return aux.conn.query("DELETE FROM categories WHERE "
                    + aux.isEqual("id", categoryId),
                    function (err, result) {
                        if (result.affectedRows)
                            return callback(204, "DELETED");
                        else
                            return callback(404, "NOT FOUND");
                    });
};


exports.dispatch = {
    GET:    getCategories,
    POST:   postCategories,
    PUT:    putCategories,
    DELETE: deleteCategories
};
